import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faIdBadge } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  paises: [];
  confirmados: [];
  recuperados: [];
  fallecidos: [];
  confirmados_pais: [];
  recuperados_pais: [];
  fallecidos_pais: [];
  loading = false;
  loading_con = false;
  loading_rec = false;
  loading_fall = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faIdBadge;
  lat = 4.800361;
  lng = 21.301793;
  zoom = 3.2;
  iconMap = {
    iconUrl: "https://i.imgur.com/hDIFjrQ.png"
  }
  constructor(private covidService: CovidService) { }

  ngOnInit() {
    this.loading = true;
    this.loading_con = true;
    this.loading_rec = true;
    this.loading_fall = true;
    this.covidService.getPaises().subscribe(
      data => {
        this.paises = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la información de los paises");
        this.loading = false;
      }
    ); 

    /*this.covidService.getConfirmados().subscribe(
      data => {
        this.confirmados = JSON.parse(data);
        this.loading_con = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        this.loading_con = false;
      }
    ); 

    this.covidService.getRecuperados().subscribe(
      data => {
        this.recuperados = JSON.parse(data);
        this.loading_rec = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        this.loading_rec = false;
      }
    );
    
    this.covidService.getFallecidos().subscribe(
      data => {
        this.fallecidos = JSON.parse(data);
        this.loading_fall = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        this.loading_fall = false;
      }
    ); 

    this.covidService.getConfirmadosPais().subscribe(
      data => {
        this.confirmados_pais = JSON.parse(data);
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        //this.loading = false;
      }
    ); 

    this.covidService.getRecuperadosPais().subscribe(
      data => {
        this.recuperados_pais = JSON.parse(data);
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        //this.loading = false;
      }
    );

    this.covidService.getFallecidosPais().subscribe(
      data => {
        this.fallecidos_pais = JSON.parse(data);
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la información de los casos confirmados");
        //this.loading = false;
      }
    ); */

  }
}
