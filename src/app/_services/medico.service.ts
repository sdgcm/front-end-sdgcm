import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://medicos-prueba.herokuapp.com/';
const API_URL = 'http://ec2-3-237-63-70.compute-1.amazonaws.com/sdgcm/api/v1/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class MedicoService {
  constructor(private http: HttpClient) { }

  getListMedicos(): Observable<any> {
    return this.http.get(API_URL + 'medicos', { responseType: 'text' });
  }

  createMedico(medico): Observable<any> {
    console.log(medico);
    return this.http.post(API_URL + 'medico', {
      cedula: medico.cedula,
      nombre: medico.nombres,
      apellidos: medico.apellidos,
      edad: medico.edad,
      fecha_nacimiento: medico.fecha,
      direccion: medico.direccion,
      telefono: medico.celular,
      especialidades: {
        id:medico.especialidad
      }
    }, httpOptions);
  }

  getMedico(id_medico:number): Observable<any> {
    return this.http.get(API_URL + 'medicos/' + id_medico, { responseType: 'text' });
  }

  /*updateMedico(id_especialidad:number, codigo:string, nombre:string, descripcion:string): Observable<any> {
    return this.http.put(API_URL + 'medicos/' + id_especialidad, {
      codigo_espc: codigo,
      nombre_espc: nombre,
      descripcion_espc: descripcion
    }, httpOptions);
    
  }*/

  deleteMedico(id_medico:number): Observable<any> {
    return this.http.delete(API_URL + 'medicos/' + id_medico, { responseType: 'text' });
  }

  medicoUser(id_medico:number, usuario:String[]): Observable<any> {
    console.log(usuario);
    return this.http.put(API_URL + 'medico/' + id_medico + "/asignar-medicos", 
      usuario
    , httpOptions);
  }
}
