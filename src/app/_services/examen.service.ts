import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://examenes-prueba.herokuapp.com/sdgcm/api/v1/';
const API_URL = 'http://ec2-3-88-183-101.compute-1.amazonaws.com/sdgcm/api/v1/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ExamenService {

  constructor(private http: HttpClient) { }

  getListExamenes(): Observable<any> {
    return this.http.get(API_URL + 'catalogo', { responseType: 'text' });
  }

  createExamen(examen): Observable<any> {
    return this.http.post(API_URL + 'catalogo', {
      nombre_examen: examen.nombre,
      descripcion_examen: examen.descripcion
    }, httpOptions);
  }

  getExamen(id_examen:number): Observable<any> {
    return this.http.get(API_URL + 'catalogo/' + id_examen, { responseType: 'text' });
  }

  deleteExamen(id_examen:number): Observable<any> {
    return this.http.delete(API_URL + 'catalogo/' + id_examen, { responseType: 'text' })
  }

}
