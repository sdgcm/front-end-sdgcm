import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://prueba-usuarios.herokuapp.com/sdgcm/api/v1/';
const API_URL = 'http://ec2-3-232-108-200.compute-1.amazonaws.com/sdgcm/api/v1/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RoleService { 

  constructor(private http: HttpClient) { }

  getListRoles(): Observable<any> {
    return this.http.get(API_URL + 'roles', { responseType: 'text' });
  }

  createRol(rol): Observable<any> {
    return this.http.post(API_URL + 'rol', {
      nombre: rol.nombre
    }, httpOptions);
  }

  getRole(id_rol:number): Observable<any> {
    return this.http.get(API_URL + 'findrol/' + id_rol, { responseType: 'text' });
  }

  updateRol(id_rol:number, rol:string): Observable<any> {
    return this.http.put(API_URL + 'updaterol/' + id_rol, {
      nombre: rol
    }, httpOptions);
    
  }

  deleteRol(id_rol:number): Observable<any> {
    return this.http.delete(API_URL + 'deleterole/' + id_rol, { responseType: 'text' })
  }

}
