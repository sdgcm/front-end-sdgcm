import { TestBed } from '@angular/core/testing';

import { HcServiceService } from './hc-service.service';

describe('HcServiceService', () => {
  let service: HcServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HcServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
