import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://pacientes-prueba.herokuapp.com/sdgcm/api/v1/';
const API_URL = 'http://ec2-54-167-32-225.compute-1.amazonaws.com/sdgcm/api/v1/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class HcServiceService {

  constructor(private http: HttpClient) { }

  getListHC(): Observable<any> {
    return this.http.get(API_URL + 'HC', { responseType: 'text' });
  }

  createHC(hc): Observable<any> {
    
    return this.http.post(API_URL + 'hccreate', {
      identificacion: hc.codigo,
      fecha_apertura: hc.fechaa
    }, httpOptions);
  }

  getHC(id_hc:number): Observable<any> {
    return this.http.get(API_URL + 'HC/' + id_hc, { responseType: 'text' });
  }

  updateHC(hc): Observable<any> {
    
    return this.http.post(API_URL + 'HCmodificar', {
      identificacion: hc.identificacion,
      fecha_apertura: hc.fechaa,
      obs_information: hc.observacion,
      fecha_atenciaon: hc.fechab
    }, httpOptions);
  }
}
