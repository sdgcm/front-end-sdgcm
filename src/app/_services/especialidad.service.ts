import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://medicos-prueba.herokuapp.com/';
const API_URL = 'http://ec2-3-237-63-70.compute-1.amazonaws.com/sdgcm/api/v1/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  constructor(private http: HttpClient) { }

  getListEspecialidades(): Observable<any> {
    return this.http.get(API_URL + 'especialidades', { responseType: 'text' });
  }

  createEspecialidad(especialidad): Observable<any> {
    return this.http.post(API_URL + 'especialidad', {
      codigo_espc: especialidad.codigo,
      nombre_espc: especialidad.nombre,
      descripcion_espc: especialidad.descripcion 
    }, httpOptions);
  }

  getEspecialidad(id_especialidad:number): Observable<any> {
    return this.http.get(API_URL + 'especialidad/' + id_especialidad, { responseType: 'text' });
  }

  deleteEspecialidad(id_especialidad:number): Observable<any> {
    return this.http.delete(API_URL + 'especialidad/' + id_especialidad, { responseType: 'text' });
  }
  
  updateEspecialidad(id_especialidad:number, codigo:string, nombre:string, descripcion:string): Observable<any> {
    return this.http.put(API_URL + 'especialidad/' + id_especialidad, {
      codigo_espc: codigo,
      nombre_espc: nombre,
      descripcion_espc: descripcion
    }, httpOptions);
    
  }
}
