import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://pacientes-prueba.herokuapp.com/sdgcm/api/v1/';
const API_URL = 'http://ec2-54-167-32-225.compute-1.amazonaws.com/sdgcm/api/v1/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class PacienteService {

  constructor(private http: HttpClient) { }

  getListPacientes(): Observable<any> {
    return this.http.get(API_URL + 'pacientes', { responseType: 'text' });
  }

  createPaciente(paciente): Observable<any> {
    
    return this.http.post(API_URL + 'pacientecreate', {
      identificacion: paciente.cedula,
      nombres: paciente.nombres,
      apellidos: paciente.apellidos,
      edad: paciente.edad,
      fecha_nacimiento: paciente.fecha,
      direccion: paciente.direccion,
      telefono: paciente.celular,
      alergia: paciente.alergia,
      historiaClinica: {
        id: paciente.hc
      }
    }, httpOptions);
  }

  getPaciente(id_pacientes:number): Observable<any> {
    return this.http.get(API_URL + 'pacientes/' + id_pacientes, { responseType: 'text' });
  }

  deletePaciente(id_paciente:number): Observable<any> {
    return this.http.delete(API_URL + 'medicos/' + id_paciente, { responseType: 'text' });
  }

  pacienteUser(id_paciente:number, usuario:String[]): Observable<any> {
    console.log(usuario);
    return this.http.put(API_URL + 'pacientes/' + id_paciente + "/asignar-pacientes", 
      usuario
    , httpOptions);
  }

}
