import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

//const API_URL = 'https://citas-prueba.herokuapp.com/';
const API_URL = 'http://ec2-54-227-143-254.compute-1.amazonaws.com/sdgcm/api/v1/';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

@Injectable({
  providedIn: 'root'
})
export class CitaService {

  constructor(private http: HttpClient) { }

  getListCitas(): Observable<any> {
    return this.http.get(API_URL + 'citas', { responseType: 'text' });
  }

  createCita(cita): Observable<any> {
    console.log(cita);
    return this.http.post(API_URL + 'citas', {
      fecha: cita.fecha,
      descripcion: cita.observacion
    }, httpOptions);
  }

  getCita(id_cita:number): Observable<any> {
    return this.http.get(API_URL + 'citas/' + id_cita, { responseType: 'text' });
  }

  eliminarCita(id_cita:number): Observable<any> {
    return this.http.delete(API_URL + 'citas/' + id_cita, { responseType: 'text' });
  }

  citaPaciente(id_medico:number, usuario): Observable<any> {
    console.log(usuario);
    return this.http.put(API_URL + 'citas/' + id_medico + "/asignar-paciente", {
      id: usuario.id
    }
    , httpOptions);
  }
}
