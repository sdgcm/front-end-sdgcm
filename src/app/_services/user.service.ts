import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CursorError } from '@angular/compiler/src/ml_parser/lexer';

const API_URL = 'http://ec2-3-232-108-200.compute-1.amazonaws.com/sdgcm/api/v1/';
//const API_URL = 'https://prueba-usuarios.herokuapp.com/sdgcm/api/v1/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getListUsers(): Observable<any> {
    return this.http.get(API_URL + 'users', { responseType: 'text' });
  }

  createUser(usuario, roles:String[]): Observable<any> {
    console.log(roles)
    return this.http.post(API_URL + 'user', {
      username: usuario.username,
      password: usuario.password,
      email: usuario.email,
      role: roles
    }, httpOptions);
  }

  getUser(id_user:number): Observable<any> {
    return this.http.get(API_URL + 'finduser/' + id_user, { responseType: 'text' });
  }

  deleteUser(id_user:number): Observable<any> {
    return this.http.delete(API_URL + 'deleteuser/' + id_user, { responseType: 'text' })
  }

  updateUser(id_user:number, user:string, correo:string, roles:String[]): Observable<any> {
    return this.http.put(API_URL + 'updateuser/' + id_user, {
      username: user,
      password: "12345",
      email: correo,
      role: roles
    }, httpOptions);
    
  }
}