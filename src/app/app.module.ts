import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';


import { AppComponent } from './app.component';
import { LoginComponent } from './_components/auth/login/login.component';
import { BoardAdminComponent } from './_components/admin/board-admin/board-admin.component';
import { ProfileComponent } from './_components/profile/profile/profile.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateUserComponent } from './_components/user/create-user/create-user.component';
import { UserDetailsComponent } from './_components/user/user-details/user-details.component';
import { UserListComponent } from './_components/user/user-list/user-list.component';
import { CommonModule } from "@angular/common";
import { RoleListComponent } from './_components/role/role-list/role-list.component';
import { CreateRolComponent } from './_components/role/create-rol/create-rol.component';
import { UpdateRolComponent } from './_components/role/update-rol/update-rol.component';
import { DeleteRoleComponent } from './_components/role/delete-role/delete-role.component';
import { DeleteUserComponent } from './_components/user/delete-user/delete-user.component';
import { UpdateUserComponent } from './_components/user/update-user/update-user.component';
import { CreateEspComponent } from './_components/especialidad/create-esp/create-esp.component';
import { DeleteEspComponent } from './_components/especialidad/delete-esp/delete-esp.component';
import { UpdateEspComponent } from './_components/especialidad/update-esp/update-esp.component';
import { EspDetailsComponent } from './_components/especialidad/esp-details/esp-details.component';
import { EspListComponent } from './_components/especialidad/esp-list/esp-list.component';
import { CreateMedicoComponent } from './_components/medico/create-medico/create-medico.component';
import { DeleteMedicoComponent } from './_components/medico/delete-medico/delete-medico.component';
import { MedicoDetailsComponent } from './_components/medico/medico-details/medico-details.component';
import { MedicoListComponent } from './_components/medico/medico-list/medico-list.component';
import { UpdateMedicoComponent } from './_components/medico/update-medico/update-medico.component';
import { BoardMedComponent } from './_components/admin/board-med/board-med.component';
import { MedicoUserComponent } from './_components/medico/medico-user/medico-user.component';
import { CreatePacienteComponent } from './_components/paciente/create-paciente/create-paciente.component';
import { PacienteDetailsComponent } from './_components/paciente/paciente-details/paciente-details.component';
import { PacienteListComponent } from './_components/paciente/paciente-list/paciente-list.component';
import { PacienteUserComponent } from './_components/paciente/paciente-user/paciente-user.component';
import { CreateExamenComponent } from './_components/examen/create-examen/create-examen.component';
import { DeleteExamenComponent } from './_components/examen/delete-examen/delete-examen.component';
import { ExamenListComponent } from './_components/examen/examen-list/examen-list.component';
import { ExamenDetailsComponent } from './_components/examen/examen-details/examen-details.component';
import { ListComponentsComponent } from './_components/dashboard/list-components/list-components.component';
import { CreateCitaComponent } from './_components/cita/create-cita/create-cita.component';
import { CitaDetailsComponent } from './_components/cita/cita-details/cita-details.component';
import { CitaListComponent } from './_components/cita/cita-list/cita-list.component';
import { DeleteCitaComponent } from './_components/cita/delete-cita/delete-cita.component';
import { CitaPacienteComponent } from './_components/cita/cita-paciente/cita-paciente.component';
import { CreateHcComponent } from './_components/hc/create-hc/create-hc.component';
import { UpdateHcComponent } from './_components/hc/update-hc/update-hc.component';
import { HcListComponent } from './_components/hc/hc-list/hc-list.component';
import { BoardPacComponent } from './_components/admin/board-pac/board-pac.component';
import { CitaListPacienteComponent } from './_components/cita/cita-list-paciente/cita-list-paciente.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BoardAdminComponent,
    ProfileComponent,
    CreateUserComponent,
    UserDetailsComponent,
    UserListComponent,
    RoleListComponent,
    CreateRolComponent,
    UpdateRolComponent,
    DeleteRoleComponent,
    DeleteUserComponent,
    UpdateUserComponent,
    CreateEspComponent,
    DeleteEspComponent,
    UpdateEspComponent,
    EspDetailsComponent,
    EspListComponent,
    CreateMedicoComponent,
    DeleteMedicoComponent,
    MedicoDetailsComponent,
    MedicoListComponent,
    UpdateMedicoComponent,
    BoardMedComponent,
    MedicoUserComponent,
    CreatePacienteComponent,
    PacienteDetailsComponent,
    PacienteListComponent,
    PacienteUserComponent,
    CreateExamenComponent,
    DeleteExamenComponent,
    ExamenListComponent,
    ExamenDetailsComponent,
    ListComponentsComponent,
    CreateCitaComponent,
    CitaDetailsComponent,
    CitaListComponent,
    DeleteCitaComponent,
    CitaPacienteComponent,
    CreateHcComponent,
    UpdateHcComponent,
    HcListComponent,
    BoardPacComponent,
    CitaListPacienteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    FontAwesomeModule,
    CommonModule, 
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDividerModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
