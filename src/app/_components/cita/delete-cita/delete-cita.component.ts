import { Component, OnInit, Inject } from '@angular/core';
import { CitaService } from '../../../_services/cita.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-cita',
  templateUrl: './delete-cita.component.html',
  styleUrls: ['./delete-cita.component.css']
})
export class DeleteCitaComponent implements OnInit {
  especialidad: any = {};
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private citService: CitaService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit(){
  }

  deleteEspecialidad() {
    this.loading_update = true;
    this.citService.eliminarCita(this.d.id).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

}
