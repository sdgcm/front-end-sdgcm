import { Component, OnInit } from '@angular/core';
import { CitaService } from '../../../_services/cita.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';


@Component({
  selector: 'app-create-cita',
  templateUrl: './create-cita.component.html',
  styleUrls: ['./create-cita.component.css']
})
export class CreateCitaComponent implements OnInit {
  form: any = {};
  pacientes: [];
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];

  constructor(private citaService: CitaService) { }

  ngOnInit() {

  }

  onSubmit() {
    this.loading = true;
    this.citaService.createCita(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
