import { Component, OnInit } from '@angular/core';
import { CitaService } from '../../../_services/cita.service';
import { CreateCitaComponent } from '../create-cita/create-cita.component';
import { CitaDetailsComponent } from '../cita-details/cita-details.component';
import { DeleteCitaComponent } from '../delete-cita/delete-cita.component';
import { CitaPacienteComponent } from '../cita-paciente/cita-paciente.component';
import { MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faFirstAid } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cita-list',
  templateUrl: './cita-list.component.html',
  styleUrls: ['./cita-list.component.css']
})
export class CitaListComponent implements OnInit {
  citas: any = {};
  pacientes = [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faFirstAid;
  vacio = false;
  constructor(private citaService: CitaService, public dialog: MatDialog, public dialog_details: MatDialog,
    public dialog_delete: MatDialog, public dialog_paciente: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.citaService.getListCitas().subscribe(
      data => {
        this.citas = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialog() {
    const dialogo = this.dialog.open(CreateCitaComponent,  { disableClose: true });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(CitaDetailsComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteCitaComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogPaciente(id: number) {
    const dialogo = this.dialog_paciente.open(CitaPacienteComponent,  { disableClose: true, data:{id: id} });
  }

}
