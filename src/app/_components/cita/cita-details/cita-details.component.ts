import { Component, OnInit, Inject} from '@angular/core';
import { CitaService } from '../../../_services/cita.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cita-details',
  templateUrl: './cita-details.component.html',
  styleUrls: ['./cita-details.component.css']
})
export class CitaDetailsComponent implements OnInit {
  cita: any = {};
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private citService: CitaService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.citService.getCita(this.d.id).subscribe(
      data => {
        this.cita = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

}
