import { Component, OnInit, Inject} from '@angular/core';
import { CitaService } from '../../../_services/cita.service';
import { PacienteService } from '../../../_services/paciente.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cita-paciente',
  templateUrl: './cita-paciente.component.html',
  styleUrls: ['./cita-paciente.component.css']
})
export class CitaPacienteComponent implements OnInit {
  paciente: any = {};
  cita: any = {};
  pacientes: [];
  users = [];
  form: any = {};
  cita_paciente = [];
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  isSuccessful = false;
  isSignUpFailed = false;
  loading_user = false;
  vacio = false;
  constructor(private citService: CitaService, private pacService: PacienteService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.vacio = false;
    this.citService.getCita(this.d.id).subscribe(
      data => {
        this.cita = JSON.parse(data);
        this.cita_paciente = this.cita.citasPaciente;
        console.log(this.cita_paciente);
        if(this.cita_paciente.length > 0){
          this.vacio = true;
        }
        this.getPacientes();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        //this.loading = false;
      }
    ); 
  }

  getPacientes(){
    this.pacService.getListPacientes().subscribe(
      data => {
        this.pacientes = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  onSubmit() {
    this.citService.citaPaciente(this.d.id, this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_user = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_user = false;
      }
    );
  }

}
