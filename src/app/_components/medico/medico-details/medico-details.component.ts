import { Component, OnInit, Inject} from '@angular/core';
import { MedicoService } from '../../../_services/medico.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-medico-details',
  templateUrl: './medico-details.component.html',
  styleUrls: ['./medico-details.component.css']
})
export class MedicoDetailsComponent implements OnInit {

  medico: any = {};
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";

  constructor(private medService: MedicoService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.medService.getMedico(this.d.id).subscribe(
      data => {
        this.medico = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

}
