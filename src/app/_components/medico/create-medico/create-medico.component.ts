import { Component, OnInit } from '@angular/core';
import { MedicoService } from '../../../_services/medico.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { EspecialidadService } from '../../../_services/especialidad.service';

@Component({
  selector: 'app-create-medico',
  templateUrl: './create-medico.component.html',
  styleUrls: ['./create-medico.component.css']
})
export class CreateMedicoComponent implements OnInit {

  form: any = {};
  especialidades: [];
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];

  constructor(private medService: MedicoService, private espService: EspecialidadService) { }

  ngOnInit() {
    this.loading = true;
    this.espService.getListEspecialidades().subscribe(
      data => {
        this.especialidades = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    );

  }

  onSubmit() {
    this.loading = true;
    this.medService.createMedico(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }


}
