import { Component, OnInit, Inject} from '@angular/core';
import { MedicoService} from '../../../_services/medico.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-medico',
  templateUrl: './delete-medico.component.html',
  styleUrls: ['./delete-medico.component.css']
})
export class DeleteMedicoComponent implements OnInit {

  medico: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private medService: MedicoService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.medService.getMedico(this.d.id).subscribe(
      data => {
        this.medico = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }
  
  deleteUser() {
    this.loading_update = true;
    this.medService.deleteMedico(this.d.id).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

}
