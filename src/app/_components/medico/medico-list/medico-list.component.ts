import { Component, OnInit } from '@angular/core';
import { MedicoService } from '../../../_services/medico.service';
import { MedicoDetailsComponent} from '../medico-details/medico-details.component';
import { CreateMedicoComponent } from '../create-medico/create-medico.component';
import { DeleteMedicoComponent } from '../delete-medico/delete-medico.component';
import { UpdateMedicoComponent } from '../update-medico/update-medico.component';
import { MedicoUserComponent } from '../medico-user/medico-user.component';
import {MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUserMd } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-medico-list',
  templateUrl: './medico-list.component.html',
  styleUrls: ['./medico-list.component.css']
})
export class MedicoListComponent implements OnInit {

  medicos: [];
  roles: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUserMd;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faAddressCard = faUserTag;

  constructor(private medicoService: MedicoService, public dialog_create: MatDialog, public dialog_details: MatDialog, public dialog_delete: MatDialog
    , public dialog_update: MatDialog, public dialog_user: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.medicoService.getListMedicos().subscribe(
      data => {
        this.medicos = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialogCreate() {
    const dialogo = this.dialog_create.open(CreateMedicoComponent,  { disableClose: true, width: "420px" });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(MedicoDetailsComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteMedicoComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogUser(id: number) {
    const dialogo = this.dialog_user.open(MedicoUserComponent,  { disableClose: true, data:{id: id} });
    //dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  /*openDialogUpdate(id: number, role: string[]) {
    const dialogo = this.dialog_update.open(UpdateMedicoComponent,  { disableClose: true, width: "380px", 
    data:{
      id: id,
      roles: role
    } });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }*/

}
