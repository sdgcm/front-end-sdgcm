import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoUserComponent } from './medico-user.component';

describe('MedicoUserComponent', () => {
  let component: MedicoUserComponent;
  let fixture: ComponentFixture<MedicoUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicoUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
