import { Component, OnInit, Inject} from '@angular/core';
import { MedicoService } from '../../../_services/medico.service';
import { UserService } from '../../../_services/user.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-medico-user',
  templateUrl: './medico-user.component.html',
  styleUrls: ['./medico-user.component.css']
})
export class MedicoUserComponent implements OnInit {
  medico: any = {};
  user: any = {};
  usuarios: [];
  users = [];
  form: any = {};
  medico_users = [];
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  isSuccessful = false;
  isSignUpFailed = false;
  loading_user = false;
  vacio = false;
  constructor(private medService: MedicoService, private userService: UserService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.vacio = false;
    this.medService.getMedico(this.d.id).subscribe(
      data => {
        this.medico = JSON.parse(data);
        this.medico_users = this.medico.usuarios;
        console.log(this.medico_users);
        if(this.medico_users.length > 0){
          this.vacio = true;
        }
        this.getUsers();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        //this.loading = false;
      }
    ); 
  }

  getUsers(){
    this.userService.getListUsers().subscribe(
      data => {
        this.usuarios = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  onSubmit() {
    this.users.push(this.form);
    this.medService.medicoUser(this.d.id, this.users).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_user = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_user = false;
      }
    );
  }

}
