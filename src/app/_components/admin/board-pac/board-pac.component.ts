import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { faUserInjured } from '@fortawesome/free-solid-svg-icons';
import { faUserMd } from '@fortawesome/free-solid-svg-icons';
import { faColumns } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faBriefcaseMedical } from '@fortawesome/free-solid-svg-icons';
import { faVials } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';
import { faStethoscope } from '@fortawesome/free-solid-svg-icons';
import { faBookMedical } from '@fortawesome/free-solid-svg-icons';
import { TokenStorageService } from '../../../_services/token-storage.service';

@Component({
  selector: 'app-board-pac',
  templateUrl: './board-pac.component.html',
  styleUrls: ['./board-pac.component.css']
})
export class BoardPacComponent implements OnInit {
  content = '';
  faUserInjured = faUserInjured;
  faUserMd = faUserMd;
  faColumns = faColumns;
  faUser = faUser;
  faBriefcaseMedical = faBriefcaseMedical;
  faVials = faVials;
  faUserTag = faUserTag;
  faStethoscope = faStethoscope;
  faBookMedical = faBookMedical;
  isLoggedIn = false;

  constructor(private userService: UserService, private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

}
