import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardPacComponent } from './board-pac.component';

describe('BoardPacComponent', () => {
  let component: BoardPacComponent;
  let fixture: ComponentFixture<BoardPacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardPacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardPacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
