import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardMedComponent } from './board-med.component';

describe('BoardMedComponent', () => {
  let component: BoardMedComponent;
  let fixture: ComponentFixture<BoardMedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardMedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardMedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
