import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { faUserInjured } from '@fortawesome/free-solid-svg-icons';
import { faUserMd } from '@fortawesome/free-solid-svg-icons';
import { faColumns } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faBriefcaseMedical } from '@fortawesome/free-solid-svg-icons';
import { faVials } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';
import { TokenStorageService } from '../../../_services/token-storage.service';

@Component({
  selector: 'app-board-med',
  templateUrl: './board-med.component.html',
  styleUrls: ['./board-med.component.css']
})
export class BoardMedComponent implements OnInit {
  content = '';
  faUserInjured = faUserInjured;
  faUserMd = faUserMd;
  faColumns = faColumns;
  faUser = faUser;
  faBriefcaseMedical = faBriefcaseMedical;
  faVials = faVials;
  faUserTag = faUserTag;
  isLoggedIn = false;
  constructor(private userService: UserService, private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

}
