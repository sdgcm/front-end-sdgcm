import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEspComponent } from './delete-esp.component';

describe('DeleteEspComponent', () => {
  let component: DeleteEspComponent;
  let fixture: ComponentFixture<DeleteEspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteEspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteEspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
