import { Component, OnInit, Inject } from '@angular/core';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-esp',
  templateUrl: './delete-esp.component.html',
  styleUrls: ['./delete-esp.component.css']
})
export class DeleteEspComponent implements OnInit {

  especialidad: any = {};
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private espService: EspecialidadService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.espService.getEspecialidad(this.d.id).subscribe(
      data => {
        this.especialidad = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }
  
  deleteEspecialidad() {
    this.loading_update = true;
    this.espService.deleteEspecialidad(this.d.id).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

}
