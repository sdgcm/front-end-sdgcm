import { Component, OnInit } from '@angular/core';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { CreateEspComponent } from '../create-esp/create-esp.component';
import { UpdateEspComponent } from '../update-esp/update-esp.component';
import { DeleteEspComponent } from '../delete-esp/delete-esp.component';
import { EspDetailsComponent } from '../esp-details/esp-details.component';
import { MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faFirstAid } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-esp-list',
  templateUrl: './esp-list.component.html',
  styleUrls: ['./esp-list.component.css']
})
export class EspListComponent implements OnInit {
  especialidades: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faFirstAid;

  constructor(private espService: EspecialidadService, public dialog: MatDialog, public dialog_update: MatDialog, public dialog_delete: MatDialog, public dialog_details: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.espService.getListEspecialidades().subscribe(
      data => {
        this.especialidades = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialog() {
    const dialogo = this.dialog.open(CreateEspComponent,  { disableClose: true });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogUpdate(id: number) {
    const dialogo = this.dialog_update.open(UpdateEspComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteEspComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }


  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(EspDetailsComponent,  { disableClose: true, data:{id: id} });
  }

}
