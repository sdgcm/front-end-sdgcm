import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspListComponent } from './esp-list.component';

describe('EspListComponent', () => {
  let component: EspListComponent;
  let fixture: ComponentFixture<EspListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
