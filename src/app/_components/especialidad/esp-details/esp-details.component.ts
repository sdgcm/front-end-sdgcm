import { Component, OnInit, Inject} from '@angular/core';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-esp-details',
  templateUrl: './esp-details.component.html',
  styleUrls: ['./esp-details.component.css']
})
export class EspDetailsComponent implements OnInit {
  especialidad: any = {};
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";

  constructor(private espService: EspecialidadService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.espService.getEspecialidad(this.d.id).subscribe(
      data => {
        this.especialidad = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

}
