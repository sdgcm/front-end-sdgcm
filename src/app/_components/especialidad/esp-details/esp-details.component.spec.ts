import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspDetailsComponent } from './esp-details.component';

describe('EspDetailsComponent', () => {
  let component: EspDetailsComponent;
  let fixture: ComponentFixture<EspDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
