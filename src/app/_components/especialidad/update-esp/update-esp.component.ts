import { Component, OnInit, Inject} from '@angular/core';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-update-esp',
  templateUrl: './update-esp.component.html',
  styleUrls: ['./update-esp.component.css']
})
export class UpdateEspComponent implements OnInit {

  especialidad: any = {};
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private espService: EspecialidadService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.espService.getEspecialidad(this.d.id).subscribe(
      data => {
        this.especialidad = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }
  
  onSubmit(value1: string, value2: string, value3: string) {
    this.loading_update = true;
    this.espService.updateEspecialidad(this.d.id, value1, value2, value3).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

}
