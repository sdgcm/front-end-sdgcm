import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEspComponent } from './update-esp.component';

describe('UpdateEspComponent', () => {
  let component: UpdateEspComponent;
  let fixture: ComponentFixture<UpdateEspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
