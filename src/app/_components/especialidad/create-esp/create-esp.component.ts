import { Component, OnInit } from '@angular/core';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-create-esp',
  templateUrl: './create-esp.component.html',
  styleUrls: ['./create-esp.component.css']
})
export class CreateEspComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "";
  constructor(private espService: EspecialidadService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.loading = true;
    this.espService.createEspecialidad(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
