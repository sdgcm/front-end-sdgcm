import { Component, OnInit, Inject} from '@angular/core';
import { ExamenService } from '../../../_services/examen.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-examen-details',
  templateUrl: './examen-details.component.html',
  styleUrls: ['./examen-details.component.css']
})
export class ExamenDetailsComponent implements OnInit {
  examen: any = {};
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";

  constructor(private exaService: ExamenService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.exaService.getExamen(this.d.id).subscribe(
      data => {
        this.examen = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

}
