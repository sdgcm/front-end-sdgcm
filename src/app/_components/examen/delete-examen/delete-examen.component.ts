import { Component, OnInit, Inject} from '@angular/core';
import { ExamenService } from '../../../_services/examen.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-examen',
  templateUrl: './delete-examen.component.html',
  styleUrls: ['./delete-examen.component.css']
})
export class DeleteExamenComponent implements OnInit {

  examen: any = {};
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private exaService: ExamenService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.exaService.getExamen(this.d.id).subscribe(
      data => {
        this.examen = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }
  
  deleteRol() {
    this.loading_update = true;
    this.exaService.deleteExamen(this.d.id).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

}
