import { Component, OnInit } from '@angular/core';
import { ExamenService } from '../../../_services/examen.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-create-examen',
  templateUrl: './create-examen.component.html',
  styleUrls: ['./create-examen.component.css']
})
export class CreateExamenComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "";
  constructor(private exaService: ExamenService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.loading = true;
    this.exaService.createExamen(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
