import { Component, OnInit } from '@angular/core';
import { ExamenService } from '../../../_services/examen.service';
import { CreateExamenComponent } from '../create-examen/create-examen.component';
import { DeleteExamenComponent } from '../delete-examen/delete-examen.component';
import { ExamenDetailsComponent } from '../examen-details/examen-details.component';
import { MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faVials } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-examen-list',
  templateUrl: './examen-list.component.html',
  styleUrls: ['./examen-list.component.css']
})
export class ExamenListComponent implements OnInit {
  examenes: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faVials;
  constructor(private exaService: ExamenService, public dialog: MatDialog, public dialog_details: MatDialog, public dialog_delete: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.exaService.getListExamenes().subscribe(
      data => {
        this.examenes = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialog() {
    const dialogo = this.dialog.open(CreateExamenComponent,  { disableClose: true });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(ExamenDetailsComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteExamenComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

}
