import { Component, OnInit } from '@angular/core';
import { MedicoService } from '../../../_services/medico.service';
import { PacienteService } from '../../../_services/paciente.service';
import { UserService } from '../../../_services/user.service';
import { EspecialidadService } from '../../../_services/especialidad.service';
import { ExamenService } from '../../../_services/examen.service';
import { RoleService } from '../../../_services/role.service';
import { CitaService } from '../../../_services/cita.service';
import { HcServiceService } from '../../../_services/hc-service.service';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUserMd } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';
import { faUserInjured } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faFirstAid } from '@fortawesome/free-solid-svg-icons';
import { faVials } from '@fortawesome/free-solid-svg-icons';
import { faStethoscope } from '@fortawesome/free-solid-svg-icons';
import { faBookMedical } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-components',
  templateUrl: './list-components.component.html',
  styleUrls: ['./list-components.component.css']
})
export class ListComponentsComponent implements OnInit {
  citas: [];
  historias: [];
  medicos: [];
  pacientes: [];
  usuarios: [];
  especialidades: [];
  examenes: [];
  roles: [];
  total_medicos: number;
  total_pacientes: number;
  total_usuarios: number;
  total_especialidades: number;
  total_examenes: number;
  total_roles: number;
  total_citas: number;
  total_historias: number;
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUserMd;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  iconPaciente = faUserInjured;
  iconUser = faUser;
  iconEsp = faFirstAid;
  iconExa = faVials;
  iconRol = faUserTag;
  iconCita = faStethoscope ;
  iconHC = faBookMedical;

  constructor(private medicoService: MedicoService, private pacienteService: PacienteService,
    private userService: UserService, private espService: EspecialidadService,
    private exaService: ExamenService, private rolService: RoleService, private citaService: CitaService,
    private hcService: HcServiceService) { }

  ngOnInit() {
    this.loading = true;
    this.medicoService.getListMedicos().subscribe(
      data => {
        this.medicos = JSON.parse(data);
        this.total_medicos = this.medicos.length;
        this.getPaciente();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getPaciente(){
    this.pacienteService.getListPacientes().subscribe(
      data => {
        this.pacientes = JSON.parse(data);
        this.total_pacientes= this.pacientes.length;
        //this.loading = false;
        this.getUsuario();
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getUsuario(){
    this.userService.getListUsers().subscribe(
      data => {
        this.usuarios = JSON.parse(data);
        this.total_usuarios= this.usuarios.length;
        //this.loading = false;
        this.getEspecialidad();
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getEspecialidad(){
    this.espService.getListEspecialidades().subscribe(
      data => {
        this.especialidades= JSON.parse(data);
        this.total_especialidades= this.especialidades.length;
        //this.loading = false;
        this.getExamen();
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getExamen(){
    this.exaService.getListExamenes().subscribe(
      data => {
        this.examenes = JSON.parse(data);
        this.total_examenes = this.examenes.length;
        //this.loading = false;
        this.getRol();
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getRol(){
    this.rolService.getListRoles().subscribe(
      data => {
        this.roles = JSON.parse(data);
        this.total_roles = this.roles.length;
        this.getCitas();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getCitas(){
    this.citaService.getListCitas().subscribe(
      data => {
        this.citas = JSON.parse(data);
        this.total_citas = this.citas.length;
        this.getHistorias();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  getHistorias(){
    this.hcService.getListHC().subscribe(
      data => {
        this.historias = JSON.parse(data);
        this.total_historias = this.historias.length;
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  

}
