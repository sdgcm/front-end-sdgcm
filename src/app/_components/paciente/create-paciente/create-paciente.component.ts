import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../../_services/paciente.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { HcServiceService } from '../../../_services/hc-service.service';


@Component({
  selector: 'app-create-paciente',
  templateUrl: './create-paciente.component.html',
  styleUrls: ['./create-paciente.component.css']
})
export class CreatePacienteComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];
  historias_clinicas: [];

  constructor(private pacService: PacienteService, private hcService: HcServiceService) { }

  ngOnInit(){
    this.loading = true;
    this.hcService.getListHC().subscribe(
      data => {
        this.historias_clinicas = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    );
  }

  onSubmit() {
    this.loading = true;
    this.pacService.createPaciente(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
