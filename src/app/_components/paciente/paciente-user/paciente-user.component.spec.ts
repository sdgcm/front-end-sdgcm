import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacienteUserComponent } from './paciente-user.component';

describe('PacienteUserComponent', () => {
  let component: PacienteUserComponent;
  let fixture: ComponentFixture<PacienteUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacienteUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacienteUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
