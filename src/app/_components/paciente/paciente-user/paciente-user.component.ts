import { Component, OnInit, Inject} from '@angular/core';
import { PacienteService } from '../../../_services/paciente.service';
import { UserService } from '../../../_services/user.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-paciente-user',
  templateUrl: './paciente-user.component.html',
  styleUrls: ['./paciente-user.component.css']
})
export class PacienteUserComponent implements OnInit {
  paciente: any = {};
  user: any = {};
  usuarios: [];
  users = [];
  form: any = {};
  medico_users = [];
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  isSuccessful = false;
  isSignUpFailed = false;
  loading_user = false;
  vacio = false;
  constructor(private pacService: PacienteService, private userService: UserService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.vacio = false;
    this.pacService.getPaciente(this.d.id).subscribe(
      data => {
        this.paciente = JSON.parse(data);
        this.medico_users = this.paciente.user;
        console.log(this.medico_users);
        if(this.medico_users.length > 0){
          this.vacio = true;
        }
        this.getUsers();
        //this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        //this.loading = false;
      }
    ); 
  }

  getUsers(){
    this.userService.getListUsers().subscribe(
      data => {
        this.usuarios = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  onSubmit() {
    this.loading_user = true;
    this.users.push(this.form);
    this.pacService.pacienteUser(this.d.id, this.users).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_user = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_user = false;
      }
    );
  }

}
