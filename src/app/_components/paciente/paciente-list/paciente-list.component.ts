import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../../_services/paciente.service';
import { PacienteDetailsComponent } from '../paciente-details/paciente-details.component';
import { CreatePacienteComponent } from '../create-paciente/create-paciente.component';
import { PacienteUserComponent } from '../paciente-user/paciente-user.component';
import {MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUserInjured } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-paciente-list',
  templateUrl: './paciente-list.component.html',
  styleUrls: ['./paciente-list.component.css']
})
export class PacienteListComponent implements OnInit {
  pacientes: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUserInjured;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faAddressCard = faUserTag;
  constructor(private pacienteService: PacienteService, public dialog_create: MatDialog, public dialog_details: MatDialog, public dialog_user: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.pacienteService.getListPacientes().subscribe(
      data => {
        this.pacientes = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialogCreate() {
    const dialogo = this.dialog_create.open(CreatePacienteComponent,  { disableClose: true, width: "420px" });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(PacienteDetailsComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogUser(id: number) {
    const dialogo = this.dialog_user.open(PacienteUserComponent,  { disableClose: true, data:{id: id} });
  }

}
