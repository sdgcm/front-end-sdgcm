import { Component, OnInit } from '@angular/core';
import { HcServiceService } from '../../../_services/hc-service.service';
import { CreateHcComponent } from '../create-hc/create-hc.component';
import { MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faFirstAid } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-hc-list',
  templateUrl: './hc-list.component.html',
  styleUrls: ['./hc-list.component.css']
})
export class HcListComponent implements OnInit {
  historias_clinicas: any = {};
  pacientes = [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faFirstAid;
  vacio = false;
  constructor(private hcService: HcServiceService, public dialog: MatDialog) { }
 
  ngOnInit() {
    this.loading = true;
    this.hcService.getListHC().subscribe(
      data => {
        this.historias_clinicas = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialog() {
    const dialogo = this.dialog.open(CreateHcComponent,  { disableClose: true });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

}
