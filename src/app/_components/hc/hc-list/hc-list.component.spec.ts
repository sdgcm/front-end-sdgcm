import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HcListComponent } from './hc-list.component';

describe('HcListComponent', () => {
  let component: HcListComponent;
  let fixture: ComponentFixture<HcListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HcListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HcListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
