import { Component, OnInit } from '@angular/core';
import { HcServiceService } from '../../../_services/hc-service.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-create-hc',
  templateUrl: './create-hc.component.html',
  styleUrls: ['./create-hc.component.css']
})
export class CreateHcComponent implements OnInit {
  form: any = {};
  pacientes: [];
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];

  constructor(private hcService: HcServiceService) { }

  ngOnInit() {

  }

  onSubmit() {
    this.loading = true;
    this.hcService.createHC(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
