import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHcComponent } from './create-hc.component';

describe('CreateHcComponent', () => {
  let component: CreateHcComponent;
  let fixture: ComponentFixture<CreateHcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
