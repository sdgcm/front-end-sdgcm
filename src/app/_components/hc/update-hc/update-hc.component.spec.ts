import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateHcComponent } from './update-hc.component';

describe('UpdateHcComponent', () => {
  let component: UpdateHcComponent;
  let fixture: ComponentFixture<UpdateHcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateHcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateHcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
