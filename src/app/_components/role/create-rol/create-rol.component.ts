import { Component, OnInit} from '@angular/core';
import { RoleService } from '../../../_services/role.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';


@Component({
  selector: 'app-create-rol',
  templateUrl: './create-rol.component.html',
  styleUrls: ['./create-rol.component.css']
})


export class CreateRolComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "";
  constructor(private roleService: RoleService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.loading = true;
    this.roleService.createRol(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

}
