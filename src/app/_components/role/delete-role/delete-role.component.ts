import { Component, OnInit, Inject} from '@angular/core';
import { RoleService } from '../../../_services/role.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.css']
})
export class DeleteRoleComponent implements OnInit {
  rol: any = {};
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  inputvalue = "hola2";
  constructor(private roleService: RoleService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.roleService.getRole(this.d.id).subscribe(
      data => {
        this.rol = JSON.parse(data);
        this.loading = false;
        console.log(this.rol);
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }
  
  deleteRol() {
    this.loading_update = true;
    this.roleService.deleteRol(this.d.id).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }
}
