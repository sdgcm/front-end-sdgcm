import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../../_services/role.service';
import { CreateRolComponent } from '../create-rol/create-rol.component';
import { UpdateRolComponent } from '../update-rol/update-rol.component';
import { DeleteRoleComponent } from '../delete-role/delete-role.component';
import { MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faIdBadge } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
  roles: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faUserTag = faIdBadge;
  constructor(private roleService: RoleService, public dialog: MatDialog, public dialog_update: MatDialog, public dialog_delete: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.roleService.getListRoles().subscribe(
      data => {
        this.roles = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialog() {
    const dialogo = this.dialog.open(CreateRolComponent,  { disableClose: true });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogUpdate(id: number) {
    const dialogo = this.dialog_update.open(UpdateRolComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteRoleComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

}
