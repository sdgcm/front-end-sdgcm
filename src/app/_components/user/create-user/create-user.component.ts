import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { RoleService } from '../../../_services/role.service';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  form: any = {};
  roles: [];
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];

  constructor(private userService: UserService, private roleService: RoleService) { }

  ngOnInit() {
    this.loading = true;
    this.roleService.getListRoles().subscribe(
      data => {
        this.roles = JSON.parse(data);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    );

  }

  onSubmit() {
    this.loading = true;
    this.userService.createUser(this.form, this.roles_seleccionados).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading = false;
      }
    );
  }

  onChange(rol: string, isChecked: boolean) {

    if(isChecked){
      this.roles_seleccionados.push(rol);
    }
    else{
      let index = this.roles_seleccionados.findIndex((element) => element == rol);
      this.roles_seleccionados.splice(index, 1);
    }
  }

}
