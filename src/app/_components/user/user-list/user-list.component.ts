import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { CreateUserComponent } from '../create-user/create-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UpdateUserComponent } from '../update-user/update-user.component';
import {MatDialog} from '@angular/material/dialog';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faPenSquare } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit { 
  //usuarios: Observable<Usuarios[]>;
  usuarios: [];
  roles: [];
  loading = false;
  faPlusCircle = faPlusCircle;
  faUser = faUser;
  faInfoCircle = faInfoCircle;
  faPenSquare = faPenSquare;
  faTrashAlt = faWindowClose;
  faAddressCard = faUserTag;

  constructor(private userService: UserService, public dialog_create: MatDialog, public dialog_details: MatDialog, public dialog_delete: MatDialog
    , public dialog_update: MatDialog) { }

  ngOnInit() {
    this.loading = true;
    this.userService.getListUsers().subscribe(
      data => {
        this.usuarios = JSON.parse(data);
        console.log(this.usuarios);
        this.loading = false;
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    ); 
  }

  openDialogCreate() {
    const dialogo = this.dialog_create.open(CreateUserComponent,  { disableClose: true, width: "380px" });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogDetails(id: number) {
    const dialogo = this.dialog_details.open(UserDetailsComponent,  { disableClose: true, data:{id: id} });
  }

  openDialogDelete(id: number) {
    const dialogo = this.dialog_delete.open(DeleteUserComponent,  { disableClose: true, data:{id: id} });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }

  openDialogUpdate(id: number, role: string[]) {
    const dialogo = this.dialog_update.open(UpdateUserComponent,  { disableClose: true, width: "380px", 
    data:{
      id: id,
      roles: role
    } });
    dialogo.afterClosed().subscribe(() => {this.ngOnInit();})
  }
}



