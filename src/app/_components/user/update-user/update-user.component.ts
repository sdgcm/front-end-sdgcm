import { Component, OnInit, Inject} from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { RoleService } from '../../../_services/role.service';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  user: any = {};
  form: any = {};
  roles: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  loading = false;
  loading_update = false;
  errorMessage = '';
  faUser = faCheckCircle;
  roles_seleccionados = [];
  roles_recuperados = [];
  constructor(private userService: UserService, private roleService: RoleService, @Inject(MAT_DIALOG_DATA) public d: any) { }

  ngOnInit() {
    this.loading = true;
    this.userService.getUser(this.d.id).subscribe(
      data => {
        this.user = JSON.parse(data);
        this.loading = false;
       
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    );
    this.roleService.getListRoles().subscribe(
      data => {
        this.roles = JSON.parse(data);
        this.roles.forEach(element => {
          this.roles_recuperados.push(element.nombre);
          
        });
        this.loading = false;

        this.d.roles.forEach(element => {
          let index = this.roles_recuperados.findIndex((x) => x == element.nombre);
          console.log(index);
          this.roles_recuperados.splice(index, 1);
          this.roles_seleccionados.push(element.nombre);
        });
      
        
      },
      err => {
        alert("No se pudo obtener la data");
        this.loading = false;
      }
    );

    console.log(this.roles_recuperados);

  }


  onSubmit(username: string, email: string) {
    this.loading_update = true;
    this.userService.updateUser(this.d.id, username, email, this.roles_seleccionados).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.loading_update = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.loading_update = false;
      }
    );
  }

  onChange(rol: string, isChecked: boolean) {

    if(isChecked){
      this.roles_seleccionados.push(rol);
    }
    else{
      let index = this.roles_seleccionados.findIndex((element) => element == rol);
      this.roles_seleccionados.splice(index, 1);
    }
  }

}
