import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './_components/auth/login/login.component';
import { ProfileComponent } from './_components/profile/profile/profile.component';
import { UserListComponent } from './_components/user/user-list/user-list.component';
import { RoleListComponent } from './_components/role/role-list/role-list.component';
import { EspListComponent } from './_components/especialidad/esp-list/esp-list.component';
import { MedicoListComponent } from './_components/medico/medico-list/medico-list.component';
import { PacienteListComponent } from './_components/paciente/paciente-list/paciente-list.component';
import { ExamenListComponent } from './_components/examen/examen-list/examen-list.component';
import { CitaListComponent } from './_components/cita/cita-list/cita-list.component';
import { CitaListPacienteComponent } from './_components/cita/cita-list-paciente/cita-list-paciente.component';
import { HcListComponent } from './_components/hc/hc-list/hc-list.component';
import { ListComponentsComponent } from './_components/dashboard/list-components/list-components.component';
import { BoardAdminComponent } from './_components/admin/board-admin/board-admin.component';
import { BoardMedComponent } from './_components/admin/board-med/board-med.component';
import { BoardPacComponent } from './_components/admin/board-pac/board-pac.component';

const routes: Routes = [
  { path: 'sgcm', component: LoginComponent},
  { path: 'profile', component: ProfileComponent },
  { path: 'admin', component: BoardAdminComponent, 
  children:[
    {path: '', component:ListComponentsComponent},
    {path: 'dashboard', component:ListComponentsComponent},
    {path: 'users', component:UserListComponent},
    {path: 'roles', component:RoleListComponent},
    {path: 'especialidades', component:EspListComponent},
    {path: 'medicos', component:MedicoListComponent},
    {path: 'pacientes', component:PacienteListComponent},
    {path: 'examenes', component:ExamenListComponent},
    {path: 'citas', component:CitaListComponent},
    {path: 'historias-clinicas', component:HcListComponent}
  ] },
  { path: 'med', component: BoardMedComponent,
  children:[
    {path: '', component:ProfileComponent},
    {path: 'dashboard', component:UserListComponent},
    {path: 'users', component:UserListComponent},
    {path: 'roles', component:RoleListComponent},
    {path: 'especialidades', component:EspListComponent},
    {path: 'medicos', component:MedicoListComponent},
  ]},
  { path: 'pac', component: BoardPacComponent,
  children:[
    {path: '', component:ProfileComponent},
    {path: 'dashboard', component:ProfileComponent},
    {path: 'citas', component:CitaListPacienteComponent}
  ]},
  
  { path: '', redirectTo: 'sgcm', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
