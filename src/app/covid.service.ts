import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/query/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class CovidService {

  constructor(private http: HttpClient) { }

  getPaises(): Observable<any> {
    return this.http.get(API_URL + 'paises', { responseType: 'text' });
  }

  getConfirmados(): Observable<any> {
    return this.http.get(API_URL + 'confirmados', { responseType: 'text' });
  }

  getRecuperados(): Observable<any> {
    return this.http.get(API_URL + 'recuperados', { responseType: 'text' });
  }

  getFallecidos(): Observable<any> {
    return this.http.get(API_URL + 'fallecidos', { responseType: 'text' });
  }

  getConfirmadosPais(): Observable<any> {
    return this.http.get(API_URL + 'confirmados_pais', { responseType: 'text' });
  }

  getRecuperadosPais(): Observable<any> {
    return this.http.get(API_URL + 'recuperados_pais', { responseType: 'text' });
  }

  getFallecidosPais(): Observable<any> {
    return this.http.get(API_URL + 'fallecidos_pais', { responseType: 'text' });
  }
}
